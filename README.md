
## Project overview

**This project gives you ability to:**

1. Upload Data From Local storage to Bucket Using CRUD Package. 

2. Using Airflow (listener) and CloudFunction (CF) Transfer data from Bucket To Updated_Bucket if original file has been changed.

3. Using CloudFunction Transfer data from Updated_Bucket to Cloud PostgresSQL (Only *.csv obviously). 


## Getting Started

In the GCP account, CloudFunctions and Airflow DAG are already in place with two buckets : "working__bucket" for manipulating files with CRUD package and
"updated__bucket" where updated files will be transferred to from "working__bucket" using DAG.
Also, there is postgresql instance with ID: "updated-data-storage" with database "updated_data" ; here updated csv file data will be loaded.

---
###Requirements:

* pip installed
* Connection to GCP
  * I will invite you 
* Service account key (JSON)
  * You can download it after invitation or ask me to send it to you 
  
  

### Steps

1. Clone repo on you local machine:

   ```git clone https://__tekle@bitbucket.org/__tekle/final_project.git```

2. Install dependencies provided in requirements.txt file:

   ```pip install -r requirements.txt ```



### Usage
**Using CRUD package** you can **create**, **read**, **update** and **delete** files into the bucket. 

First you need to import class:

   ```from CRUD_service.crud_service import BucketCrudService ```

Then instantiate with arguments: project name, bucket name and credentials path 

   ```obj = BucketCrudService(project_name=project, bucket_name=bucket_name, cred_path=cred_path)```


For creating file in the bucket, you can use the function with arguments: file path and blob name(optional)

   ```obj.create_file(file_path, blob_name)```

For reading file from the bucket, you can use the function with arguments: blob name and destination directory path(optional). If destination path is passed as argument, function will download the file into this directory. If not, function will return bytes object of that file from bucket 

   ```obj.read_file(blob_name, destination_dir)```



For updating file in the bucket, you can use the function with arguments: file path and blob name(optional)

   ```obj.update_file(file_path, blob_name)```



For deleting file from the bucket, you can use the function with argument: blob name

   ```obj.create_file(file_path, blob_name)```



To see data uploaded into cloud psql, you can connect to base using 
In GCP to see uploaded data into psql, you will need to go to instance named "updated-data-storage" with user: "postgres", password: "password" and database : "updated_data" ; open the cloud shell and type command:
   
   ```gcloud sql connect updated-data-storage --user=postgres```

To connect to database use : ```\c updated_data```

Or you can create another database, in this case you need you provide database name in variable in the CloudFunction "transfer_to_postgres".

Maximum size of file to update is around 217M .


 ---
## Tools used

* GCP
  * Cloud storage
  * Cloud Composer
  * CloudFunctions
  * Cloud psql
* Airflow

## Author

Tekle Macharadze


