import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def upload(conn, df, table_name):
    """Loads dataframe data to psql database

    :param conn: connection
    :param df: Dataframe
    :param table_name: string
    :return:
    """

    try:
        logger.debug(f'Uploading data to postgres database ...')
        df.to_sql(table_name, conn, if_exists='replace', index=False,
                  chunksize=1000, method='multi')
        logger.debug(
            f'Successfully uploaded data to postgresql {table_name}')
        return True
    except Exception as err:
        logger.error(f"Couldn't load data to database. reason : {err}")
        return False
