import pandas as pd
import logging
from datetime import datetime
from .connection import connection_to_psql
from .upload_to_db import upload

logging.basicConfig(level=logging.DEBUG)


def data_to_psql(event):
    """Triggered by a change to a Cloud Storage bucket - 'updated__bucket'.
      uploading data from csv file to psql database
    Args:
         event (dict): Event payload.
    """
    connection_name = 'final-project-346313:europe-central2:' \
                      'updated-data-storage'
    db_user = "postgres"
    db_pass = "password"
    db_name = "updated_data"
    db_port = 5432
    db_hostname = '34.116.219.242'
    bucket_name = 'updated__bucket'
    file_with_extension = event["name"]
    filename, extension = file_with_extension.split('.')

    if extension == 'csv':
        now = datetime.now().strftime("%d_%m_%Y_%H:%M:%S")
        table_name = f'{filename}_{now}'
        conn = connection_to_psql(connection_name,
                                  db_user, db_pass,
                                  db_hostname, db_port, db_name)

        df = pd.read_csv(f'gs://{bucket_name}/{file_with_extension}',
                         engine="pyarrow")
        """Idea - to check if csv header exists - 
        to avoid error from this reason"""
        upload(conn, df, table_name)
    else:
        logging.debug(
            "Can't upload data to psql: updated file has no extension 'csv'")
