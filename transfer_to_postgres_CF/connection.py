import sqlalchemy
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def connection_to_psql(
        connection_name, db_user, db_pass, db_hostname, db_port, db_name):
    """Connects to postgresql and returns connection

    :param connection_name: string
    :param db_user: string
    :param db_pass: string
    :param db_hostname: string
    :param db_port: int
    :param db_name: string
    :return: connection
    """

    query_string = dict(
        {"unix_sock": "/cloudsql/{}/.s.PGSQL.5432".format(connection_name)})

    try:
        conn = sqlalchemy.create_engine(
            sqlalchemy.engine.url.URL(
                drivername="postgresql+pg8000",
                username=db_user,
                password=db_pass,
                host=db_hostname,
                port=db_port,
                database=db_name,
                query=query_string
            )
        )
        logger.debug('Successfully connected to postgresql.')
        return conn
    except Exception as err:
        logger.error(f"Couldn't connect to database. reason : {err}")
        return None
