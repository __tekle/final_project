from google.cloud import storage
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class GCPHandlers:
    """Class for methods which helps to manipulate gcp bucket.
    Used for creating storage client, getting bucket and blobs from it.

    For initialization requires bucket name and path to
    service account json file.
    """

    def __init__(self, bucket_name, cred_path):
        self.cred_path = cred_path
        self.bucket_name = bucket_name
        self.client = self.get_client()
        self.bucket = self.get_bucket()

    def get_client(self):
        """Creates google cloud storage client using service account
        credentials

        :return: client object
        """
        try:
            client = storage.Client.from_service_account_json(self.cred_path)
            logger.debug(f'Created client ')
            return client
        except Exception as err:
            logger.error(f"Couldn't return client: {err}")
            return None

    def get_bucket(self):
        """Creates bucket using client

        :return: bucket object
        """
        try:
            bucket = self.client.get_bucket(self.bucket_name)
            logger.debug(f'Created bucket')
            return bucket
        except Exception as err:
            logger.error(f"Couldn't return bucket: {err}")
            return None

    def blob_from_bucket(self, blob_name: str):
        """Returns blob of specific bucket using .blob() method

        :param blob_name: string
        :return: blob object
        """
        try:
            bucket = self.bucket
            blob = bucket.blob(blob_name)
            logger.debug(' Blob from bucket ')
            return blob
        except Exception as err:
            logger.error(f"Couldn't create blob: {err}")
            return None

    def get_blob_from_bucket(self, blob_name: str):
        """Returns blob of specific bucket sing .get_blob() method.

        :param blob_name: string
        :return: blob object
        """
        try:
            bucket = self.bucket
            blob = bucket.get_blob(blob_name)
            logger.debug('Got blob from bucket')
            return blob
        except Exception as err:
            logger.error(f"Couldn't create blob: {err}")
            return None

    def blobs_list_from_bucket(self):
        """Returns blobs list of specific bucket

        :return: blobs list object
        """
        try:
            bucket = self.bucket
            blobs = bucket.list_blobs()
            logger.debug('Received blobs list from bucket.')
            return blobs
        except Exception as err:
            logger.error(f"Couldn't receive blobs list: {err}")
            return None

    def blob_existence_checker(self, blob_name):
        """Checks whether blob with passed name exists in bucket or not.

        :param blob_name: string
        :return: bool
        """
        logger.debug(blob_name)
        blobs = self.blobs_list_from_bucket()
        blobs = [b.name for b in blobs]
        if blob_name not in blobs:
            return False
        else:
            return True

