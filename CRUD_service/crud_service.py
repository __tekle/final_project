import logging
from datetime import datetime
from .helpers import GCPHandlers

logging.basicConfig(level=logging.DEBUG)


class BucketCrudService:
    """ Class provides methods for CRUD operations on Google cloud bucket.

    For initialization needs path to service account json file,
    project name and bucket name.
     """

    def __init__(self, cred_path: str, project_name: str, bucket_name: str):
        self.cred_path = cred_path
        self.project_name = project_name
        self.bucket_name = bucket_name
        self.gcp_handlers = GCPHandlers(self.bucket_name, self.cred_path)

    def create_file(self, file_path: str, blob_name: str = None):
        """creates blob in specific bucket (uploads file from local).
        Uses filepath of desired file and blob name

        :param file_path: string (file path)
        :param blob_name: (optional) string (blob name)
        :return: bool
        """

        try:
            if blob_name:
                blob = self.gcp_handlers.blob_from_bucket(blob_name)
            else:
                blob = \
                    self.gcp_handlers.blob_from_bucket(
                        file_path.split('/')[-1])
            blob_exists = self.gcp_handlers.blob_existence_checker(blob.name)
            if not blob_exists:
                blob.upload_from_filename(file_path)
                logging.debug(f'Uploaded file into bucket.')
                return True
            else:
                logging.warning(f'Blob with the name "{blob.name}" '
                                f'already exists in the bucket named '
                                f'"{self.bucket_name}."')
                logging.debug(f'File was not uploaded.')
                return False
        except Exception as err:
            logging.error(f"Couldn't upload file to bucket : {err}")
            return False

    def read_file(self, blob_name: str, destination_dir: str = None):
        """Reads file from bucket: downloads file

        :param blob_name: string (blob name)
        :param destination_dir: string (destination dir path)
        :return: file in bytes
        """
        try:
            blob = self.gcp_handlers.blob_from_bucket(blob_name)
            blob_exists = self.gcp_handlers.blob_existence_checker(blob.name)
            if blob_exists:
                if not destination_dir:
                    logging.info(f"Returning file as bytes object.")
                    return blob.download_as_bytes()
                else:
                    blob.download_to_filename(f'{destination_dir}/{blob_name}')
                    logging.info(f"File downloaded into"
                                 f" {destination_dir}/{blob_name}.")
                    return True
            else:
                logging.error(f"Blob with name {blob.name} doesn't exist "
                              f"in bucket {self.bucket_name}.")
        except Exception as err:
            logging.error(f"Couldn't read file from bucket: {err}")
            return None

    def update_file(self, file_path: str, blob_name: str = None):
        """updates existing blob on bucket based on their hash,
            needs path of changed file and file name on bucket (blob name)
            if file content is changed, metadata will be changed also

        :param file_path: string (file path)
        :param blob_name: string (blob name)
        :return: bool
        """
        try:
            if blob_name:
                blob = self.gcp_handlers.get_blob_from_bucket(blob_name)
            else:
                blob = self.gcp_handlers.get_blob_from_bucket(
                    file_path.split('/')[-1])
            if blob:
                old_hash = blob.md5_hash
                blob.delete()
                blob.upload_from_filename(file_path)
                new_hash = blob.md5_hash
                logging.debug(f'old hash: {old_hash} and new hash: {new_hash}')

                if old_hash != new_hash:
                    blob.metadata = {'updated': datetime.now()}
                    blob.patch()
                    logging.info(
                        f'Cloud function "trigger_dag" has been triggered.')
                else:
                    logging.warning(
                        f'File contains same data. Dag was not triggered')

                logging.debug('Updated file in bucket ')
                return True
            else:
                logging.error(f"Such blob doesn't exist "
                              f"in the bucket {self.bucket_name}.")
        except Exception as err:
            logging.error(f"Couldn't update file to bucket : {err}")
            return False

    def delete_file(self, blob_name: str):
        """Deletes specified file from bucket.

        :param blob_name: string (file name on bucket)
        :return: bool
        """
        try:
            blob = self.gcp_handlers.blob_from_bucket(blob_name)
            blob_exists = self.gcp_handlers.blob_existence_checker(blob.name)
            if blob_exists:
                blob.delete()
                logging.debug('Deleted file from bucket.')
                return True
            else:
                logging.error(f"Blob with name {blob.name} doesn't exist "
                              f"in bucket {self.bucket_name}.")
        except Exception as err:
            logging.error(f"Couldn't delete file to bucket: {err}")
            return False
