from datetime import datetime
from airflow import DAG
from airflow.models import Variable
from airflow.providers.google.cloud.transfers.gcs_to_gcs \
    import GCSToGCSOperator
from airflow.operators.python import PythonOperator

src_bucket = 'working__bucket'
dest_bucket = 'updated__bucket'


def filename_to_var(ds, **kwargs):
    """Getting file name from dag and set it to the airflow variable."""

    name = kwargs['dag_run'].conf['name']
    Variable.set("filename", name)


with DAG('files_transport',
         schedule_interval=None,
         start_date=datetime(2022, 3, 6)) as dag:
    set_filename_to_variable = PythonOperator(
        task_id='gcs_filename_to_variable',
        python_callable=filename_to_var, )

    file_to_bucket = GCSToGCSOperator(
        task_id="transfer_data",
        source_bucket=src_bucket,
        source_object=f'{Variable.get("filename")}',
        destination_bucket=dest_bucket,
        destination_object=f'{Variable.get("filename")}',
        dag=dag)

set_filename_to_variable >> file_to_bucket
