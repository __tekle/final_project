import composer2_airflow_rest_api


def trigger_dag_gcf(data):
    """
    Function is triggered by changing metadata of file in bucket ,
     in this case - "working__bucket".
    Trigger a DAG and pass event data.

    Args:
      data: A dictionary containing the data for the event. Its format depends
      on the event.

    For more information about the arguments, see:
https://cloud.google.com/functions/docs/writing/background#function_parameters
    """

    web_server_url = (
     "https://6ceaa8f24a644a46a94137d9657f5c45-dot-europe-central2.composer"
        ".googleusercontent.com"
    )

    dag_id = 'files_transport'

    composer2_airflow_rest_api.trigger_dag(web_server_url, dag_id, data)

